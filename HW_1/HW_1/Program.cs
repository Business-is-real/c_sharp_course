﻿using System;
using System.Runtime.CompilerServices;
using static System.Console;

namespace HW_1
{
    class Program
    {
        static void Main()
        {
            #region HOMEWORK 1
            //// Task 1
            //WriteLine("Hello world!!!");
            //ReadKey();

            //// Task 2
            //WriteLine("Hello world!!!");
            //Write("Hello ");
            //Write("world");
            //Write("\n!!!\n");
            //ReadKey();
            #endregion

            #region DateTime
            //DateTime date = new DateTime(2024, 03, 18, 12, 43, 05);
            //Write(date.ToString());
            #endregion

            #region Output in slender columns name1, name2, name3

            //string name1 = "Maksim";
            //string surname1 = "Ivanov";
            //string name2 = "Ivan";
            //string surname2 = "Popov";
            //string name3 = "Andrey";
            //string surname3 = "Bolotov";
            //string name4 = "Petr";
            //string surname4 = "Neelov";
            //string name5 = "Ilya";
            //string surname5 = "Kuleshov";



            //Write($"{name1, -10} {surname1, -10}\n");
            //Write($"{name2,-10} {surname2,-10}\n");
            //Write($"{name3,-10} {surname3,-10}\n");
            //Write($"{name4,-10} {surname4,-10}\n");
            //Write($"{name5,-10} {surname5,-10}\n");

            #endregion

            #region Convert <bool> to <int>
            bool flag = true;
            int value = Convert.ToInt32(flag);
            Write(value);

            #endregion
        }
    }
}
