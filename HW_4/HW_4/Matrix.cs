﻿using System;

namespace HW_4
{
    class Matrix
    {
        static void Main()
        {
            #region Task 1
            // Случайная матрица

            Console.WriteLine("Hello, lets create 2d array)\n");
            Matrix matrix = new Matrix();
            
            int[,]matrix1 =  matrix.CreateArray2DByUserInput();
            Random random = new Random();
            matrix.FillArray2D(matrix1, random);
            Console.WriteLine("\n---Matrix 1---\n");
            Print2DMatrix(matrix1);
            PrintSumOfAllElements(matrix1);
            Console.Write("Press enter: ");
            Console.ReadKey();

            #endregion

            #region Task 2
            // Сложение матриц

            int[,]matrix2 = matrix.CreateArray2DByUserInput();
            matrix.FillArray2D(matrix2, random);
            
            Console.WriteLine("\n---Matrix 2---\n");
            Print2DMatrix(matrix2);
            Console.Write("Press enter: ");
            Console.ReadKey();

            Console.WriteLine("\n\n---Matrix 1---\n");
            Print2DMatrix(matrix1);
            Console.WriteLine("---PLUS Matrix 2---\n");
            Print2DMatrix(matrix2);
            
            int[,] newMatrix =  matrix.Addition(matrix1, matrix2);
            
            Console.WriteLine("---SUM---\n");
            Print2DMatrix(newMatrix);
            Console.ReadKey();
            
            #endregion
        }

        int[,] CreateArray2DByUserInput()
        {
            Console.Write("Enter the quantity of rows in the array: ");
            int rows = Convert.ToInt32(Console.ReadLine());
            Console.Write("And now enter the columns quantity please: ");
            int columns = Convert.ToInt32(Console.ReadLine());
            int[,] matrix = new int[rows, columns];
            return matrix;
        }
        void FillArray2D(int[,] array, Random rnd)
        {
            for (int i = 0; i < array.GetLength(0); i++) // размерность 1
            {
                for (int j = 0; j < array.GetLength(1); j++) // размерность 2
                {
                    array[i, j] = rnd.Next(10, 100); // только двузначные числа от 10 до 99
                }
            }
        }
        static void Print2DMatrix(int[,] array)
        {
            for (int i = 0; i < array.GetLength(0); i++) // размерность 1
            {
                for (int j = 0; j < array.GetLength(1); j++) // размерность 2
                {
                    Console.Write($"{array[i, j], -4} "); // вывод первой строки
                }
                Console.WriteLine(); // перевзод на следующую строку
            }
            Console.WriteLine();
        }
        
        static void PrintSumOfAllElements(int[,] array)
        {
            int sum = 0;

            foreach (var item in array)
            {
                sum += item;
            }
            Console.WriteLine(sum);
        }

        int[,] Addition(int[,] matrix1, int[,]matrix2)
        {
            if (matrix1.GetLength(0) != matrix2.GetLength(0) || matrix1.GetLength(1) != matrix2.GetLength(1))
                throw new Exception("It is impossible to add up the matrices");
            
            int[,] newMatrix = new int[matrix1.GetLength(0), matrix1.GetLength(1)];

            for (int i = 0; i < matrix1.GetLength(0); i++)
            {
                for (int j = 0; j < matrix2.GetLength(1); j++)
                {
                    newMatrix[i, j] = matrix1[i,j] + matrix2[i,j];
                }
            }
            return newMatrix;
        }
    }
}
