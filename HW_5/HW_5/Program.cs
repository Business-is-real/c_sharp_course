﻿using System;
using System.Text;

namespace HW_5
{
    class Program
    {
        static void Main()
        {
            #region Task 1

            //Console.Write("Введите любое предложение: ");
            //string userInput = Console.ReadLine();
            //string[] splittedText =  SplitText(userInput);
            //splittedText.Reverse();
            //PrintText(splittedText);

            #endregion

            #region Task 2

            Console.Write("Введите длинное предложение: ");
            string inputPhrase = Console.ReadLine();
            Console.WriteLine(ReversWords(inputPhrase));
            Console.ReadKey();

            #endregion
        }
        /// <summary>
        /// Разделяет строку на отдельные слова
        /// </summary>
        /// <param name="text"></param>
        /// <returns> string[] </returns>
        static string[] SplitText(string text)
        {
            return text.Split();
        }

        /// <summary>
        /// Выводит каждый элемент массива типа String на новой строке
        /// </summary>
        /// <param name="text"></param>
        static void PrintText(string[] text)
        {
            foreach (var item in text)
            {
                Console.WriteLine(item);
            }
        }

        /// <summary>
        /// Разворачивает слова в предложении
        /// </summary>
        /// <param name="inputPhrase"></param>
        /// <returns> string.Trim() </returns>
        static string ReversWords(string inputPhrase)
        {
            string splitted = Reverse(inputPhrase);
            return splitted.Trim();
        }

        /// <summary>
        /// Формирует новую строку с помощью StringBuilder
        /// и возвращает обычную строку типа string
        /// </summary>
        /// <param name="text"></param>
        /// <returns> string </returns>
        static string Reverse(string text)
        {
            string[] tmp = text.Split();
            StringBuilder sb = new StringBuilder();

            for (int i = tmp.Length - 1; i >= 0; i--)
            {
                sb.Append(tmp[i]);
                sb.Append(" ");
            }
            return sb.ToString();
        }
    }
}