﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;

namespace HW_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Task 1
            // Приложение по определению чётного или нечётного числа

            Console.Write("Enter an integer number, please: ");
            int useriInput = int.Parse(Console.ReadLine());

            if (useriInput % 2 == 0) Console.WriteLine("this is an evem number!");
            else Console.WriteLine("This is an odd number!");
            Console.ReadKey();

            #endregion

            #region Task 2
            // Программа подсчёта суммы карт в игре «21»

            Console.WriteLine("Hello, Let`s play in '21'. I`ll remind you some rules...\n\n" +
                "for cards without pictures enter their numbers, otherwise:\n\n" +
                "for 'jack'  enter J\nfor 'queen' enter Q\nfor 'king'  enter K\nfor 'ace'   enter T");

            bool play = true;
            int counter = 0;

            while (play)
            {
                Console.Write("\nHow many game cards do you have? ");

                try { counter = int.Parse(Console.ReadLine()); }
                catch { counter--; }

                int sum = 0;

                for (int i = 0; i < counter; i++)
                {
                    Console.Write($"Enter your card value number {i + 1} according to the rules: ");
                    string input = Console.ReadLine().ToUpper();

                    switch (input)
                    {
                        case "6": sum += 6; break;
                        case "7": sum += 7; break;
                        case "8": sum += 8; break;
                        case "9": sum += 9; break;
                        case "10": sum += 10; break;
                        case "J": sum += 10; break;
                        case "Q": sum += 10; break;
                        case "K": sum += 10; break;
                        case "T": sum += 10; break;
                        default:
                            Console.WriteLine("You entered something wrong, try again!");
                            i--; break;
                    }
                }

                if (counter == 0) Console.WriteLine("You don't have any game cards!");
                else if (counter < 0) Console.WriteLine("you should have entered a positive number!");
                else Console.WriteLine($"Sum of your cards = {sum}");

                Console.Write("for exit press \"q\": ");
                if (Console.ReadLine() == "q") play = false;
            }
            #endregion

            #region Task 3
            //Проверка простого числа

            Console.Write("Enter a number: ");
            int input = int.Parse(Console.ReadLine());
            bool flag = false;

            int start = 2;
            if (input > 1) flag = true;

            while (flag)
            {
                if (input == 2) break;
                else if (input % start == 0)
                {
                    flag = false;
                    break;
                }
                start++;
                if (input == start) break;
            }
            if (!flag) Console.WriteLine("This is a composite number");
            else Console.WriteLine("This is a prime number");

            #endregion

            #region Task 4
            //Наименьший элемент в последовательности

            int minValue = int.MaxValue;
            Console.Write("Enter the length of the sequence numbers: ");
            int length = int.Parse(Console.ReadLine());
            if (length < 0) Console.WriteLine("Number should be positive!");
            int counter = 0;

            while (counter < length)
            {
                Console.Write("Enter a number: ");

                int input = int.Parse(Console.ReadLine());
                if (input < minValue) minValue = input;
                counter++;
            }
            if (minValue == int.MaxValue) Console.WriteLine("Try again next time!");
            else Console.WriteLine($"The minimal value = {minValue}");

            #endregion

            #region Task 5
            // Игра «Угадай число»

            Console.Write("Enter maximum value: ");
            int maxValue = int.Parse(Console.ReadLine());
            Random random = new Random();
            int number = random.Next(0, maxValue+1);

            Console.Write("Try to guess what number I made up)) or press Enter to quit: ");
            

            while (true)
            {

                string userIpnut = Console.ReadLine();
                if (userIpnut.Equals(string.Empty))
                {
                    Console.WriteLine($"Number was {number}");
                    break;
                }
                else if (Convert.ToInt32(userIpnut) < number) Console.Write("Bigger than you think) Next attempt: ");
                else if (Convert.ToInt32(userIpnut) > number) Console.Write("Smaller than you think) Next attempt: ");
                else
                {
                    Console.WriteLine($"You are right, number is {number}!");
                    Console.ReadKey();
                    break;
                }
            }

            #endregion
        }
    }
}
