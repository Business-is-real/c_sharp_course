﻿using System;
using System.IO;
using System.Text;

namespace HW_6
{
    class Program
    {
        public static string employeePath = "bd.txt";
        public static string employeeIdPath = "id.txt";
        public static int employeeId;
        public static bool flag = true;

        static void Main()
        {
            
            while (flag)
            {
                Console.WriteLine("1 - показать список\n2 - добавить сотрудника\n3 - выйти");

                switch (Convert.ToInt32(Console.ReadLine()))
                {
                    case 1:
                        Show();
                        break;
                    case 2:
                        Add();
                        break;
                    case 3:
                        Exit();
                        break;
                    default:
                        Console.WriteLine("\nНеверный выбор\n");
                        break;
                }
            }
        }

        private static void Exit()
        {
            flag = false;
            Console.WriteLine("Работа завершена");
        }

        private static void Add()
        {
            if (!File.Exists("bd.txt"))
            {
                File.Create("bd.txt").Close();
                StreamWriter sw = new StreamWriter(employeeIdPath);
                sw.WriteLine(employeeId);
                sw.Close();
            }

            int id = Convert.ToInt32( File.ReadAllText(employeeIdPath));
            StringBuilder employee = new StringBuilder(50);
            employee.Append(++id);
            employee.Append('#');
            DateTime date = DateTime.Now;
            employee.Append(date);
            employee.Append('#');
            Console.Write("Введите Ф.И.О. ");
            employee.Append(Console.ReadLine());
            employee.Append('#');
            Console.Write("Введите возраст ");
            employee.Append(Console.ReadLine());
            employee.Append('#');
            Console.Write("Введите пол ");
            employee.Append(Console.ReadLine());
            employee.Append('#');
            Console.Write("Введите рост ");
            employee.Append(Console.ReadLine());
            employee.Append('#');
            Console.Write("Введите дату рождения ");
            employee.Append(Console.ReadLine());
            employee.Append('#');
            Console.Write("Введите место рождения ");
            employee.Append(Console.ReadLine());
            employee.Append('\n');
            File.WriteAllText(employeeIdPath, id.ToString());

            using (StreamWriter sw = new StreamWriter(employeePath, true))
            {
                sw.Write(employee);
            }
        }

        private static void Show()
        {
            {
                if (!File.Exists("bd.txt"))
                {
                    File.Create("bd.txt").Close();
                    StreamWriter sw = new StreamWriter(employeeIdPath);
                    sw.WriteLine(employeeId);
                    sw.Close();
                    
                    Console.WriteLine($"\nФайл создан {DateTime.Now}\n");
                }
                else using (StreamReader sr = new StreamReader(employeePath))
                {
                    StringBuilder sb = new StringBuilder();
                    while (!sr.EndOfStream) 
                    {
                        sb.Append(sr.ReadLine().Replace('#',' '));
                        sb.Append('\n');
                    }
                    if(sb.Length == 0) Console.WriteLine("\nНет записей");
                    else Console.WriteLine(sb);
                }
            }
        }
    }
}
