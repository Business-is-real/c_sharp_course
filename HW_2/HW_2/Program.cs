﻿using System;

namespace HW_2
{
    class Program
    {
        static void Main()
        {
            #region Task 1
            string fullName = "Rybakov Alexandr Vladimirovich";
            byte age = 41;
            string email = "DaKinder@outlook.com";
            float programmingScores = 67.5f;
            float mathScores = 45.0f;
            float physicsScores = 29.9f;

            Console.WriteLine(
                $"full name: {fullName}\nage: {age}, email: {email}\n" +
                $"programming scores: {programmingScores} points\n" +
                $"math scores: {mathScores} points\n" +
                $"physics scores: {physicsScores} points");
            Console.ReadKey();
            #endregion

            #region Task 2
            float totalScore = programmingScores + mathScores + physicsScores;
            Console.WriteLine("\ntotal score: {0}", totalScore);
            float averageScore = totalScore / 3;
            Console.WriteLine("average score: {0}", averageScore.ToString("#.##"));
            Console.ReadKey();
            #endregion
        }
    }
}
